-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2023 at 06:11 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `j0403`
--

-- --------------------------------------------------------

--
-- Table structure for table `cau_hoi`
--

CREATE TABLE `cau_hoi` (
  `id` int(11) NOT NULL,
  `diem_moi_cau_hoi` int(11) NOT NULL,
  `mon_hoc` varchar(255) DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `noi_dung_cau_hoi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `cau_hoi`
--

INSERT INTO `cau_hoi` (`id`, `diem_moi_cau_hoi`, `mon_hoc`, `ngay_tao`, `noi_dung_cau_hoi`) VALUES
(1, 2, 'Hóa học', '2023-04-29 17:00:00', 'Câu 1: Si tác dụng với chất nào sau đây ở nhiệt độ thường ???'),
(2, 2, 'Hóa học', '2023-04-29 17:00:00', 'Câu 2: Trong phản ứng nào sau đây, silic có tính oxi hóa ?'),
(3, 2, 'Hóa học', '2023-04-29 17:00:00', 'Câu 3. Phản ứng nào sau đây là sai ?'),
(4, 3, 'Hóa học', '2023-04-27 17:00:00', 'Câu 4. Thủy tinh lỏng là'),
(5, 4, 'Hóa học', '2023-04-28 17:00:00', 'Câu 5. Si đioxit phản ứng được với tất cả các chất trong dãy sau đây ?'),
(6, 2, 'Hóa học', '2023-04-30 07:54:51', 'Câu 6. Phản ứng hữu cơ thường xảy ra chậm do');

-- --------------------------------------------------------

--
-- Table structure for table `dap_an`
--

CREATE TABLE `dap_an` (
  `id` int(11) NOT NULL,
  `dap_an` varchar(255) DEFAULT NULL,
  `giai_thich` varchar(255) DEFAULT NULL,
  `noi_dung_dap_an` varchar(255) DEFAULT NULL,
  `cau_hoi_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `dap_an`
--

INSERT INTO `dap_an` (`id`, `dap_an`, `giai_thich`, `noi_dung_dap_an`, `cau_hoi_id`) VALUES
(1, 'đúng', 'abcxyz', 'A - F2', 1),
(2, 'sai', 'abcxyz', 'B - O2', 1),
(3, 'sai', 'abcxyz', 'C - H2', 1),
(4, 'sai', 'abcxyz', 'D - Mg', 1),
(5, 'sai', 'abcxyz', 'A. Si + 2F2 → SiF4', 2),
(6, 'sai', 'abcxyz', 'A. Chất hữu cơ có nhiệt độ nóng chảy thấp', 2),
(7, 'đúng', 'abcxyz', 'C. 2Mg + Si → Mg2Si', 2),
(8, 'sai', 'abcxyz', 'D. Si + O2 → SiO2', 2),
(9, 'sai', 'abcxyz', 'A. SiO2 + 2C → 2CO + Si', 3),
(10, 'đúng', 'abcxyz', 'B. SiO2 + 4HCl → SiCl4 + 2H2O', 3),
(11, 'sai', 'abcxyz', 'C. SiO2 + 4HF → SiF4 + 2H2O', 3),
(12, 'sai', 'abcxyz', 'D. SiO2 + 2Mg → 2MgO + Si', 3),
(13, 'sai', 'abcxyz', 'A. silic đioxit nóng chảy.', 4),
(14, 'đúng', 'abcxyz', 'B. dung dịch đặc của Na2SiO3 và K2SiO3 .', 4),
(15, 'sai', 'abcxyz', 'C. dung dịch bão hòa của axit silixic.', 4),
(16, 'sai', 'abcxyz', 'D. thạch anh nóng chảy.', 4),
(17, 'sai', 'abcxyz', 'A. NaOH, MgO, HCl', 5),
(18, 'sai', 'abcxyz', 'B. KOH, MgCO3, HF', 5),
(19, 'đúng', 'abcxyz', 'C. NaOH, Mg, HF', 5),
(20, 'sai', 'abcxyz', 'D. KOH, Mg, HCl', 5),
(29, 'sai', 'abcxyz', 'A. chất hữu cơ dễ bay hơi', 6),
(30, 'sai', 'abcxyz', 'B. liên kết trong hợp chất hữu cơ bền', 6),
(31, 'đúng', 'abcxyz', 'C. liên kết trong phân tử các chất hữu cơ ít phân cực nên khó bị phân cắt', 6),
(33, 'sai', 'abcxyz', 'D. Các liên kết trong phân tử hợp chất hữu cơ có độ bền khác nhau', 6);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(38);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cau_hoi`
--
ALTER TABLE `cau_hoi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_j655ixm4c8fncalop9r3nq4vr` (`noi_dung_cau_hoi`);

--
-- Indexes for table `dap_an`
--
ALTER TABLE `dap_an`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_bl7ceuwjrh4lcp6ef0mhvo4r3` (`noi_dung_dap_an`),
  ADD KEY `FKj0s20fdm30ekbd3s70ylbjs18` (`cau_hoi_id`);

--
-- Indexes for table `hibernate_sequence`
--
ALTER TABLE `hibernate_sequence`
  ADD PRIMARY KEY (`next_val`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hibernate_sequence`
--
ALTER TABLE `hibernate_sequence`
  MODIFY `next_val` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dap_an`
--
ALTER TABLE `dap_an`
  ADD CONSTRAINT `FKj0s20fdm30ekbd3s70ylbjs18` FOREIGN KEY (`cau_hoi_id`) REFERENCES `cau_hoi` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
