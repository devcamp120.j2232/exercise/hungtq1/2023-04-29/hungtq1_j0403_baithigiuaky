package com.devcamp.j0403.thi_trac_nghiem.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "cau_hoi")

public class CauHoi {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull(message = "Input of the question's content is required")
    @Column(name = "noi_dung_cau_hoi", unique = true)
    private String noiDungCauHoi;

    @Column(name = "diem_moi_cau_hoi")
    @NotNull(message = "Input of the question's mark is required")
    @Range(min=1, max=10, message = "The mark must be from 1 to 10")
    private int diemMoiCauHoi;

    @Column(name = "mon_hoc")
    @NotEmpty(message = "Input of the subject is required")
    private String monHoc;

    @Column(name = "ngay_tao", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date ngayTao;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = DapAn.class)
    @JoinColumn(name = "cau_hoi_id")
    @JsonIgnore
    private Set<DapAn> dapAn;

    public CauHoi() {
    }

    public CauHoi(int id, String noiDungCauHoi, int diemMoiCauHoi, String monHoc, Date ngayTao, Set<DapAn> dapAn) {
        this.id = id;
        this.noiDungCauHoi = noiDungCauHoi;
        this.diemMoiCauHoi = diemMoiCauHoi;
        this.monHoc = monHoc;
        this.ngayTao = ngayTao;
        this.dapAn = dapAn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoiDungCauHoi() {
        return noiDungCauHoi;
    }

    public void setNoiDungCauHoi(String noiDungCauHoi) {
        this.noiDungCauHoi = noiDungCauHoi;
    }

    public int getDiemMoiCauHoi() {
        return diemMoiCauHoi;
    }

    public void setDiemMoiCauHoi(int diemMoiCauHoi) {
        this.diemMoiCauHoi = diemMoiCauHoi;
    }

    public String getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(String monHoc) {
        this.monHoc = monHoc;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Set<DapAn> getDapAn() {
        return dapAn;
    }

    public void setDapAn(Set<DapAn> dapAn) {
        this.dapAn = dapAn;
    }
   
    
    
}
