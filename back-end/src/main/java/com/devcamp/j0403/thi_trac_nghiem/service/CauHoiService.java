package com.devcamp.j0403.thi_trac_nghiem.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j0403.thi_trac_nghiem.model.CauHoi;
import com.devcamp.j0403.thi_trac_nghiem.repository.CauHoiRepository;

@Service
public class CauHoiService {
    
    @Autowired
    CauHoiRepository cauHoiRepository;

    public ArrayList<CauHoi> getCauHoiList(){
        ArrayList<CauHoi> listCauHoi = new ArrayList<>();
        cauHoiRepository.findAll().forEach(listCauHoi::add);
        return listCauHoi;
    }

    public CauHoi getCauHoiById(int id){
        CauHoi cauHoi = cauHoiRepository.findById(id);
        if(cauHoi != null){
            return cauHoi;
        }else{
            return null;
        }
    }

}
