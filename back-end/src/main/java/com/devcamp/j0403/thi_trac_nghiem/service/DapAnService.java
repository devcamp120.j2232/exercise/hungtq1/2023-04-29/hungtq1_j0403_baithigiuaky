package com.devcamp.j0403.thi_trac_nghiem.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j0403.thi_trac_nghiem.model.CauHoi;
import com.devcamp.j0403.thi_trac_nghiem.model.DapAn;
import com.devcamp.j0403.thi_trac_nghiem.repository.CauHoiRepository;
import com.devcamp.j0403.thi_trac_nghiem.repository.DapAnRepository;

@Service
public class DapAnService {
    
    @Autowired
    DapAnRepository dapAnRepository;

    @Autowired
    CauHoiRepository cauHoiRepository;

    public ArrayList<DapAn> getDapAnList(){
        ArrayList<DapAn> listDapAn = new ArrayList<>();
        dapAnRepository.findAll().forEach(listDapAn::add);
        return listDapAn;
    }

    public DapAn getDapAnById(int id){
        DapAn dapAn = dapAnRepository.findById(id);
        if(dapAn != null){
            return dapAn;
        }
        return null;
    }


    public Set<DapAn> getDapAnByCauHoiId(int id){
        CauHoi cauHoi = cauHoiRepository.findById(id);
        if(cauHoi != null){
            return cauHoi.getDapAn();
        }
        return null;
    }

}
