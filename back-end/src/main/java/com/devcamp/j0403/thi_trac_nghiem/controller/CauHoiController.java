package com.devcamp.j0403.thi_trac_nghiem.controller;

import java.util.*;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j0403.thi_trac_nghiem.model.CauHoi;
import com.devcamp.j0403.thi_trac_nghiem.repository.CauHoiRepository;
import com.devcamp.j0403.thi_trac_nghiem.service.CauHoiService;

@RequestMapping("/")
@CrossOrigin
@RestController
public class CauHoiController {
 
    @Autowired
    CauHoiRepository cauHoiRepository;

    @Autowired
    CauHoiService cauHoiService;

    @GetMapping("/all-question")
    public ResponseEntity<List<CauHoi>> getAllCauHoi(){
        try {
            List<CauHoi> listCauHoi = cauHoiService.getCauHoiList();
            return new ResponseEntity<>(listCauHoi, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/question/{id}")
    public ResponseEntity<Object> getCauHoiById(@PathVariable("id") int id){
        try {
            CauHoi cauHoi = cauHoiService.getCauHoiById(id);
            if(cauHoi != null){
                return new ResponseEntity<>(cauHoi, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create-question")
    public ResponseEntity<Object> createCauHoi(@Valid @RequestBody CauHoi pCauHoi){
        try {
            CauHoi cauHoi = new CauHoi();
            cauHoi.setNoiDungCauHoi(pCauHoi.getNoiDungCauHoi());
            cauHoi.setDiemMoiCauHoi(pCauHoi.getDiemMoiCauHoi());
            cauHoi.setMonHoc(pCauHoi.getMonHoc());
            cauHoi.setNgayTao(new Date());
            return new ResponseEntity<>(cauHoiRepository.save(cauHoi), HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to CREATE specified Question: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update-question/{id}")
    public ResponseEntity<Object> updateCauHoi(@PathVariable("id") int id, @Valid @RequestBody CauHoi pCauHoi){
        try {
            CauHoi cauHoi = cauHoiRepository.findById(id);
            if(cauHoi != null){
                cauHoi.setNoiDungCauHoi(pCauHoi.getNoiDungCauHoi());
                cauHoi.setDiemMoiCauHoi(pCauHoi.getDiemMoiCauHoi());
                cauHoi.setMonHoc(pCauHoi.getMonHoc());
                return new ResponseEntity<>(cauHoiRepository.save(cauHoi), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++:::::" + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to UPDATE specified Question: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/delete-question/{id}")
    public ResponseEntity<Object> deleteCauHoi(@PathVariable("id") int id){
        try {
            //cauHoiRepository.deleteById(id);
            //return new ResponseEntity<>(HttpStatus.NO_CONTENT);
           CauHoi cauHoi = cauHoiRepository.findById(id);
            if(cauHoi != null){
                cauHoiRepository.deleteById(id);
                return new ResponseEntity<>( HttpStatus.NO_CONTENT);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
