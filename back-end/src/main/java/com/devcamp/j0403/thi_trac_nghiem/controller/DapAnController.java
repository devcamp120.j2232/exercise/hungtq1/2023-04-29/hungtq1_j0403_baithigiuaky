package com.devcamp.j0403.thi_trac_nghiem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j0403.thi_trac_nghiem.model.CauHoi;
import com.devcamp.j0403.thi_trac_nghiem.model.DapAn;
import com.devcamp.j0403.thi_trac_nghiem.repository.CauHoiRepository;
import com.devcamp.j0403.thi_trac_nghiem.repository.DapAnRepository;
import com.devcamp.j0403.thi_trac_nghiem.service.DapAnService;

@RequestMapping("/")
@CrossOrigin
@RestController
public class DapAnController {
    
    @Autowired
    DapAnRepository dapAnRepository;

    @Autowired
    DapAnService dapAnService;

    @Autowired
    CauHoiRepository cauHoiRepository;

    @GetMapping("/all-answer")
    public ResponseEntity<List<DapAn>> getAllDapAn(){
        try {
            List<DapAn> listDapAn  = dapAnService.getDapAnList();
            return new ResponseEntity<>(listDapAn, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/answer/{id}")
    public ResponseEntity<Object> getDapAnById(@PathVariable("id") int id){
        try {
            DapAn dapAn =  dapAnService.getDapAnById(id);
            if (dapAn != null){
                return new ResponseEntity<>(dapAn, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create-answer/{id}")
    public ResponseEntity<Object> createDapAn(@PathVariable("id") int id, @Valid  @RequestBody DapAn pDapAn){
        try {
            CauHoi cauHoi = cauHoiRepository.findById(id);
            if(cauHoi != null){            
                DapAn dapAn = new DapAn();
                dapAn.setNoiDungDapAn(pDapAn.getNoiDungDapAn());
                dapAn.setDapAn(pDapAn.getDapAn());
                dapAn.setGiaiThich(pDapAn.getGiaiThich());
                dapAn.setCauHoi(cauHoi);
                return new ResponseEntity<>(dapAnRepository.save(dapAn), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to CREATE specified Answer: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update-answer/{dapAnId}/{cauHoiId}")
    public ResponseEntity<Object> updateDapAn(@PathVariable(value = "dapAnId") int dapAnId, @PathVariable(value = "cauHoiId") int cauHoiId, @Valid  @RequestBody DapAn pDapAn){
        try {
            DapAn dapAn = dapAnRepository.findById(dapAnId);
            CauHoi cauHoi = cauHoiRepository.findById(cauHoiId);
            if(dapAn != null){
                dapAn.setNoiDungDapAn(pDapAn.getNoiDungDapAn());
                dapAn.setDapAn(pDapAn.getDapAn());
                dapAn.setGiaiThich(pDapAn.getGiaiThich());
                dapAn.setCauHoi(cauHoi);
                return new ResponseEntity<>(dapAnRepository.save(dapAn), HttpStatus.OK);
            }else{
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
           return ResponseEntity.unprocessableEntity().body("Failed to UPDATE specified Answer: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/delete-answer/{id}")
    public ResponseEntity<Object> deleteDapAn(@PathVariable("id") int id){
        try {
            dapAnRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);    
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
