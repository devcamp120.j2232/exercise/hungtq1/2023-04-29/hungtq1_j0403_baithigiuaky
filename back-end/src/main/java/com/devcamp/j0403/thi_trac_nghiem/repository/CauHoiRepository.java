package com.devcamp.j0403.thi_trac_nghiem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j0403.thi_trac_nghiem.model.CauHoi;

public interface CauHoiRepository extends JpaRepository<CauHoi, Integer> {
    CauHoi findById(int id);
}
