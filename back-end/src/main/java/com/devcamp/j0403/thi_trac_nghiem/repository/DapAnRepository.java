package com.devcamp.j0403.thi_trac_nghiem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j0403.thi_trac_nghiem.model.DapAn;

public interface DapAnRepository extends JpaRepository<DapAn, Integer> {
    DapAn findById(int id);
}
