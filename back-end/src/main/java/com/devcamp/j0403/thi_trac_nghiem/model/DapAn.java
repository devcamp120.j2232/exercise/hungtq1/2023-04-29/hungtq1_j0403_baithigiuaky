package com.devcamp.j0403.thi_trac_nghiem.model;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "dap_an")
public class DapAn {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "noi_dung_dap_an", unique = true)
    @NotEmpty(message = "Input of the answer's content is required")
    private String noiDungDapAn;

    @Column(name = "dap_an")
    @NotEmpty(message = "Input of the answer is required")
    private String dapAn;

    @Column(name = "giai_thich")
    @NotEmpty(message = "Input of the answer's explanation is required")
    private String giaiThich;

    @ManyToOne
    //@JsonIgnore
    private CauHoi cauHoi;

    public DapAn() {
    }

    public DapAn(int id, String noiDungDapAn, String dapAn, String giaiThich, CauHoi cauHoi) {
        this.id = id;
        this.noiDungDapAn = noiDungDapAn;
        this.dapAn = dapAn;
        this.giaiThich = giaiThich;
        this.cauHoi = cauHoi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoiDungDapAn() {
        return noiDungDapAn;
    }

    public void setNoiDungDapAn(String noiDungDapAn) {
        this.noiDungDapAn = noiDungDapAn;
    }

    public String getDapAn() {
        return dapAn;
    }

    public void setDapAn(String dapAn) {
        this.dapAn = dapAn;
    }

    public String getGiaiThich() {
        return giaiThich;
    }

    public void setGiaiThich(String giaiThich) {
        this.giaiThich = giaiThich;
    }

    public CauHoi getCauHoi() {
        return cauHoi;
    }

    public void setCauHoi(CauHoi cauHoi) {
        this.cauHoi = cauHoi;
    }

    
    
}
