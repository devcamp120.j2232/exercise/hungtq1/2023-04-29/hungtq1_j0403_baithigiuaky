package com.devcamp.j0403.thi_trac_nghiem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThiTracNghiemApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThiTracNghiemApplication.class, args);
	}

}
